import 'intl';
import 'intl/locale-data/jsonp/en';
import React, {useEffect} from 'react';
import SplashScreen from 'react-native-splash-screen';
// UI Kitten
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';

import AppNavigation from './navigtion';
import store from '../src/redux/store';
import {Provider} from 'react-redux';

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <Provider store={store}>
          <AppNavigation />
        </Provider>
      </ApplicationProvider>
    </>
  );
};

export default App;
