import rootReducer from './reducer/reducers';
import {createStore} from 'redux';

const store = createStore(rootReducer);

export default store;
