// todo reducer
/**
 *
 * @param state
 *
 * @param action
 * {
 *      type: string;
 *      id: string;
 *      text: string;
 *      completed: boolean;
 * }
 */
const todos = (state = [], action: any) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [...state, {id: action.id, text: action.text, completed: false}];
    case 'DETELE_TODO':
      return state.filter(({id}) => id !== action.id);
    default:
      return state;
  }
};

export default todos;
