const ItemState = {
  item: [],
};
// action chứa cái type( bắt buộc ) và dữ liệu cần thiết để xưr lý state
const todoData = (state = [], action: any) => {
  switch (action.type) {
    case 'ADD_DATA':
      return [...state, ...action.data];
    // case 'DELETE_DATA ':
    //   return [...state];
    case 'DETELE_DATA':
      return state.filter(({id}) => id !== action.id);
    case 'ADD_ITEM':
      return [...state, action.item];
    // edit
    // lấy cái id ( like )
    default:
      return state;
  }
};
export default todoData;
