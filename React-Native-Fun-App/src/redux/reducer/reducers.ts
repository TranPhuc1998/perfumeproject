import {combineReducers} from 'redux';
import todo from '../../redux/reducer/todo';
import todoData from './todoData';

export default combineReducers({
  stateTodo: todo,
  stateData: todoData,
  //  stateDatar: user,
  // stateNews: news,
});
