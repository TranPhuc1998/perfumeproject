let nextTodoId = 0;
//action gửi tới reducers, reducers chỉ tiếp nhận
// so sánh

export const addTodo = (text: any) => ({
  type: 'ADD_TODO',
  id: nextTodoId++,
  text,
});
export const deleteTodo = (id: any) => ({
  type: 'DETELE_TODO',
  id,
});
export const deleteData = (id: string) => ({
  type: 'DETELE_DATA',
  id,
});
export const addData = (data: any) => ({
  type: 'ADD_DATA', // mô tả hanhg động để cho reducers biết
  data,
});
export const addItem = (item: Object) => ({
  type: 'ADD_ITEM',
  item,
});
// export const addItem = (
//   data: any[],
//   branchName: string,
//   productName: string,
//   productDescription: string,
//   productPrice: any,
// ) => ({
//   type: 'ADD_DATA',
//   data,
//   branchName,
//   productName,
//   productDescription,
//   productPrice,
// });
// export const addItem = () => {
//   ItemState;
// };
