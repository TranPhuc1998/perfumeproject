import {
  BottomTabBarOptions,
  BottomTabBarProps,
} from '@react-navigation/bottom-tabs';
import React, {Fragment, useMemo, useCallback} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Animated,
  SafeAreaView,
  StyleSheet,
  Text,
  Linking,
  Alert,
  Button,
} from 'react-native';
import Svg, {Path} from 'react-native-svg';

const width = Dimensions.get('window').width;
interface IConact {}
interface IOpen {
  url: any;
  children: any;
}

const ContactScreen: React.FC<IConact> = ({}) => {
  return (
    <View style={styles.bodyHome}>
      <Image
        style={{
          resizeMode: 'contain',
          width: 300,
          height: 300,
          borderRadius: 600 / 2,

          justifyContent: 'center',
          marginTop: 20,
        }}
        source={require('../../Assets//Images/contact.png')}
      />

      <TouchableOpacity style={styles.buttonImage}>
        <Text
          style={styles.textButton}
          onPress={() => {
            Linking.openURL('https://www.facebook.com/CallmeChay98/');
          }}>
          Contact FB
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttonImage2}>
        <Text
          style={styles.textButton2}
          onPress={() => {
            Linking.openURL('https://www.instagram.com/01.thanggg12/');
          }}>
          Contact Instagram
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ContactScreen;
const styles = StyleSheet.create({
  bodyHome: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  tinyLogo: {
    backgroundColor: 'pink',
  },
  buttonImage: {
    flexDirection: 'row',
    width: width,
    backgroundColor: '#FF3366',
    padding: 20,
    marginTop: 30,
  },
  textButton: {
    marginLeft: 160,
  },
  textButton2: {
    marginLeft: 160,
  },
  buttonImage2: {
    flexDirection: 'row',
    width: width,
    backgroundColor: '#993399',
    padding: 20,
    marginTop: 30,
  },
});
